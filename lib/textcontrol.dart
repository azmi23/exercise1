import 'package:flutter/material.dart';

class TextControll extends StatelessWidget {
  final Function handler;

  TextControll({this.handler});
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: RaisedButton(
        child: Text('Change Text'),
        color: Colors.blue,
        textColor: Colors.white,
        onPressed: () => handler(),
      ),
    );
  }
}
