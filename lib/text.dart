import 'package:flutter/material.dart';

class ShowText extends StatelessWidget {
  final String text;

  ShowText(this.text);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Text(
        text,
        textAlign: TextAlign.center,
      ),
    );
  }
}
