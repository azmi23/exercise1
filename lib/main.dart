// 1) Create a new Flutter App (in this project) and output an AppBar and some text
// below it
// 2) Add a button which changes the text (to any other text of your choice)
// 3) Split the app into three widgets: App, TextControl & Text

import 'package:flutter/material.dart';

import './text.dart';
import './textcontrol.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  var _textArr = ['Hello', 'My Name is', 'Azmi'];
  var _indexText = 0;

  void _changeIndex() {
    if (_indexText < _textArr.length - 1) {
      setState(() {
        _indexText += 1;
      });
    } else {
      setState(() {
        _indexText = 0;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Text('Text Assginment'),
          ),
          body: Container(
              padding: EdgeInsets.all(10),
              child: Column(children: <Widget>[
                ShowText(_textArr[_indexText]),
                TextControll(
                  handler: _changeIndex,
                )
              ]))),
    );
  }
}
